package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSTile;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class WalkToBank extends Node{

	private final RSTile bankTile = new RSTile(3253, 3420, 0);
	@Override
	public boolean isValid() {
		return !Vars.isAtExperiments() && (Inventory.getCount(Vars.food) == 0 || Inventory.getCount(Vars.VARROCK_TELE_NAME) == 0) && !Banking.isInBank();
	}

	@Override
	public void execute() {
		WebWalking.walkTo(bankTile, new Condition() {
			
			@Override
			public boolean active() {
				return Banking.isInBank();
			}
		}, General.random(3000, 3500));
	}
	
	@Override
	public String toString()
	{
		return "Walking to bank!";
	}

}
