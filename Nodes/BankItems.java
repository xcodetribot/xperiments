package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Banking;
import org.tribot.api2007.Inventory;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class BankItems extends Node{

	@Override
	public boolean isValid() {
		return Banking.isInBank() && (
				(!Vars.useAttackPotions || (Vars.useAttackPotions && Inventory.getCount(Vars.ATTACK_POTION_NAMES[0]) == 0)) ||
				(!Vars.useStrengthPotions || (Vars.useStrengthPotions && Inventory.getCount(Vars.STRENGTH_POTION_NAMES[0]) == 0)) ||
				Inventory.getCount(Vars.food) == 0 ||
				Inventory.getCount(Vars.VARROCK_TELE_NAME) == 0 );
	}

	@Override
	public void execute() {
		if(!Banking.isBankScreenOpen())
		{
			Banking.openBank();
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(10, 30);
					return Banking.isBankScreenOpen();
				}
			}, General.random(1500, 2000));
		}
		if(Banking.isBankScreenOpen())
		{
			Banking.depositAllExcept(Vars.food, Vars.VARROCK_TELE_NAME, Vars.ATTACK_POTION_NAMES[0], Vars.STRENGTH_POTION_NAMES[0]);
			if(Vars.useAttackPotions && Inventory.getCount(Vars.ATTACK_POTION_NAMES[0]) == 0)
			{
				Banking.withdraw(1, Vars.ATTACK_POTION_NAMES[0]);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Inventory.getCount(Vars.ATTACK_POTION_NAMES[0]) > 0;
					}
				}, General.random(800, 1200));
			}
			if(Vars.useStrengthPotions && Inventory.getCount(Vars.STRENGTH_POTION_NAMES[0]) == 0)
			{
				Banking.withdraw(1, Vars.STRENGTH_POTION_NAMES[0]);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Inventory.getCount(Vars.STRENGTH_POTION_NAMES[0]) > 0;
					}
				}, General.random(800, 1200));
			}
			if(Inventory.getCount(Vars.VARROCK_TELE_NAME) == 0)
			{
				Banking.withdraw(1, Vars.VARROCK_TELE_NAME);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Inventory.getCount(Vars.VARROCK_TELE_NAME) > 0;
					}
				}, General.random(800, 1200));
			}
			if(!Inventory.isFull())
			{
				Banking.withdraw(0, Vars.food);
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Inventory.getCount(Vars.food) > 0;
					}
				}, General.random(800, 1200));
			}
		}
	}
	
	@Override
	public String toString()
	{
		return "Banking";
	}

}
