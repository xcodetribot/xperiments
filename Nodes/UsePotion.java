package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSItem;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class UsePotion extends Node{

	@Override
	public boolean isValid() {
		return Player.getRSPlayer().isInCombat() && (
				(Vars.useStrengthPotions && Skills.getCurrentLevel(SKILLS.STRENGTH) == Skills.getActualLevel(SKILLS.STRENGTH) && Inventory.getCount(Vars.STRENGTH_POTION_NAMES) > 0) ||
				(Vars.useAttackPotions && Skills.getCurrentLevel(SKILLS.ATTACK) == Skills.getActualLevel(SKILLS.ATTACK) && Inventory.getCount(Vars.ATTACK_POTION_NAMES) > 0)
				);
	}

	@Override
	public void execute() {
		if(Vars.useStrengthPotions && Skills.getCurrentLevel(SKILLS.STRENGTH) == Skills.getActualLevel(SKILLS.STRENGTH))
			drinkPotion(SKILLS.STRENGTH);
		if(Vars.useAttackPotions && Skills.getCurrentLevel(SKILLS.ATTACK) == Skills.getActualLevel(SKILLS.ATTACK))
			drinkPotion(SKILLS.ATTACK);
	}
	
	private void drinkPotion(final SKILLS skill)
	{
		RSItem[] pots = Inventory.find(skill == SKILLS.STRENGTH ? Vars.STRENGTH_POTION_NAMES : Vars.ATTACK_POTION_NAMES);
		if(pots.length > 0)
		{
			if(pots[0].click("Drink"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Skills.getCurrentLevel(skill) > Skills.getActualLevel(skill) && Player.getAnimation() == -1;
					}
				}, General.random(2000, 2500));
			}
		}
	}
	
	@Override
	public String toString()
	{
		return "Drinking potions";
	}

}
