package scripts.Xperiments.Nodes;

import org.tribot.api2007.Game;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.api2007.types.RSNPC;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class Antiban extends Node{
	@Override
	public boolean isValid() {
		return Player.getRSPlayer().isInCombat();
	}

	@Override
	public void execute() {
		
		if(Vars.ANTIBAN.BOOL_TRACKER.HOVER_NEXT.next())
		{
			RSNPC[] experiments = NPCs.findNearest(Vars.EXPERIMENTS_FILTER);
			for(RSNPC e : experiments)
			{
				if(e.isOnScreen() && e.hover())
				{
					Vars.ANTIBAN.BOOL_TRACKER.HOVER_NEXT.reset();
				}
					break;
			}
		}
		
		Vars.ANTIBAN.performCombatCheck();

		Vars.ANTIBAN.performEquipmentCheck();

		Vars.ANTIBAN.performExamineObject();

		Vars.ANTIBAN.performFriendsCheck();

		Vars.ANTIBAN.performLeaveGame();

		Vars.ANTIBAN.performMusicCheck();

		Vars.ANTIBAN.performPickupMouse();

		Vars.ANTIBAN.performQuestsCheck();

		Vars.ANTIBAN.performRandomMouseMovement();

		Vars.ANTIBAN.performRandomRightClick();

		Vars.ANTIBAN.performRotateCamera();
		
		Vars.ANTIBAN.performTimedActions(SKILLS.STRENGTH);

		Vars.ANTIBAN.performXPCheck(SKILLS.STRENGTH);
		
		if(Player.isMoving() && Game.getRunEnergy() >= Vars.ANTIBAN.INT_TRACKER.NEXT_RUN_AT.next())
		{
			Options.setRunOn(true);
			Vars.ANTIBAN.INT_TRACKER.NEXT_RUN_AT.reset();
		}
	}
	
	@Override
	public String toString()
	{
		return "Fighting";
	}
}
