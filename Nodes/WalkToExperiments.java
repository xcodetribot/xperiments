package scripts.Xperiments.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSObject;
import org.tribot.api2007.types.RSObjectDefinition;
import org.tribot.api2007.types.RSTile;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class WalkToExperiments extends Node{

	final RSTile 	point1 = new RSTile(3405, 3506, 0),
					point2 = new RSTile(3440, 9887, 0),
					point3 = new RSTile(3578, 3523, 0);
	final RSArea underground = new RSArea(new RSTile(3402, 9906, 0),new RSTile(3450, 9881, 0));
	
	@Override
	public boolean isValid() {
		return Inventory.isFull() && Inventory.getCount(Vars.food) > 0 && Inventory.getCount(Vars.VARROCK_TELE_NAME) > 0 && !Vars.isAtExperiments();
	}

	@Override
	public void execute() {
		RSTile playerPosition = Player.getPosition();
		if(playerPosition.getX() <= 3412 && !underground.contains(playerPosition))
		{
			RSObject[] trapdoor = Objects.findNearest(5, "Trapdoor");
			if(trapdoor.length == 0)
			{
				WebWalking.walkTo(point1, new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Objects.findNearest(5, "Trapdoor").length > 0;
					}
				}, General.random(4000, 5000));
			}
			else
			{
				if(DynamicClicking.clickRSObject(trapdoor[0], "Climb-down"))
				{//
				} else if(DynamicClicking.clickRSObject(trapdoor[0], "Open"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return underground.contains(Player.getPosition());
						}
					}, General.random(3000, 3500));
				}
				
			}
		} else if(underground.contains(playerPosition))
		{
			RSObject[] barrier = Objects.findNearest(5, "Holy barrier");
			if(barrier.length == 0)
			{
				WebWalking.walkTo(point2, new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Objects.findNearest(5, "Holy barrier").length > 0;
					}
				}, General.random(3000, 3500));
			}
			else
			{
				if(DynamicClicking.clickRSObject(barrier[0], "Pass-through"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return Player.getPosition().getY() <= 3485;
						}
					}, General.random(2000, 2500));
				}
			}
			
		} else if(playerPosition.getX() >= 3423 && !underground.contains(playerPosition))
		{
			RSObject[] memorial = Objects.findNearest(6, "Memorial");
			if(memorial.length == 0)
			{
				WebWalking.walkTo(point3, new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Objects.findNearest(6, "Memorial").length > 0;
					}
				}, General.random(3000, 3500));
			}
			else
			{
				if(memorial[0].getPosition().getX() != 3578)
					return;
				if(DynamicClicking.clickRSObject(memorial[0], "Push"))
				{
					Timing.waitCondition(new Condition() {
						
						@Override
						public boolean active() {
							General.sleep(10, 30);
							return Vars.isAtExperiments();
						}
					}, General.random(1500, 2000));
				}
			}
		}
	}
	
	@Override
	public String toString()
	{
		return "Walking to experiments!";
	}

}
