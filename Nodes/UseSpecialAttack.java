package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Game;
import org.tribot.api2007.GameTab;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.Options;
import org.tribot.api2007.Player;
import org.tribot.api2007.GameTab.TABS;
import org.tribot.api2007.types.RSInterfaceChild;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;
import scripts.Xperiments.Utils.Weapon;

public class UseSpecialAttack extends Node{

	@Override
	public boolean isValid() {
		return Player.getRSPlayer().isInCombat() && Game.getSetting(300) >= (Weapon.getSpecialAttackCost() * 10) && Game.getSetting(301) != 1;
	}

	@Override
	public void execute() {
		if(GameTab.getOpen() != TABS.COMBAT)
		{
			if(GameTab.open(TABS.COMBAT))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return GameTab.getOpen() == TABS.COMBAT;
					}
				}, General.random(1200, 1500));
			}
		}
		if(GameTab.getOpen() == TABS.COMBAT)
		{
			RSInterfaceChild special = Interfaces.get(593, 33);
			if(special != null)
			{
				special.click("Use");
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Game.getSetting(301) == 1;
					}
				}, General.random(1200, 1500));
			}
			
		}
	}
	
	@Override
	public String toString()
	{
		return "Using special attack";
	}

}
