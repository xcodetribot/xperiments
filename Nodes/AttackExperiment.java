package scripts.Xperiments.Nodes;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Combat;
import org.tribot.api2007.NPCs;
import org.tribot.api2007.PathFinding;
import org.tribot.api2007.Walking;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.util.DPathNavigator;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class AttackExperiment extends Node{

	private RSNPC[] experiments = new RSNPC[]{};
	private DPathNavigator dpn = new DPathNavigator();
	
	@Override
	public boolean isValid() {
		experiments = NPCs.findNearest(Vars.EXPERIMENTS_FILTER);
		return Combat.getAttackingEntities().length == 0 && Vars.isAtExperiments();
	}

	@Override
	public void execute() {
		if(experiments.length > 0)
		{
			final RSNPC e = experiments[0];
			if(!e.isOnScreen())
			{
				dpn.setStoppingConditionCheckDelay(General.random(3500, 4200));
				dpn.setStoppingCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return DynamicClicking.clickRSNPC(e, "Attack");
					}
				});
				
				dpn.traverse(e.getPosition());
			}
			
			if(e.isOnScreen())
			{
				Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, true);
				if(DynamicClicking.clickRSNPC(e, "Attack"))
				{
				
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						General.sleep(10, 30);
						return Combat.getAttackingEntities().length > 0;
					}
				}, General.random(2000, 2400));
				Vars.lastBusyTime = Timing.currentTimeMillis();
				
				}
			}

		}
		else
		{
			dpn.setStoppingConditionCheckDelay(General.random(3500, 4200));
			dpn.setStoppingCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(10, 30);
					return NPCs.findNearest(Vars.EXPERIMENTS_FILTER).length > 0;
				}
			});
			dpn.traverse(Vars.areas[General.random(0, Vars.areas.length - 1)].getRandomTile());
		}
	}
	
	@Override
	public String toString()
	{
		return "Attacking";
	}

}
