package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.Login;
import org.tribot.api2007.types.RSItem;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class TeleportToVarrock extends Node{

	@Override
	public boolean isValid() {
		return Vars.isAtExperiments() && Inventory.getCount(Vars.food) == 0 && Combat.getHPRatio() <= Vars.ANTIBAN.INT_TRACKER.NEXT_EAT_AT.next();
	}

	@Override
	public void execute() {
		Vars.ANTIBAN.INT_TRACKER.NEXT_EAT_AT.reset();
		RSItem[] varrockTele = Inventory.find(Vars.VARROCK_TELE_NAME);
		if(varrockTele.length > 0)
		{
			varrockTele[0].click("Break");
			Timing.waitCondition(new Condition() {
				
				@Override
				public boolean active() {
					General.sleep(10, 30);
					return !Vars.isAtExperiments();
				}
			}, General.random(6000, 6500));
		}
		else
		{
			Vars.stop = true;
			Login.logout();
			General.println("Stopping script. Can't find Varrock Teleports :(");
		}
	}
	
	@Override
	public String toString()
	{
		return "Teleporting";
	}

}
