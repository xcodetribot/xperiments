package scripts.Xperiments.Nodes;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Inventory;
import org.tribot.api2007.types.RSItem;

import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

public class EatFood extends Node{
	@Override
	public boolean isValid() {
		return Combat.getHPRatio() <= Vars.ANTIBAN.INT_TRACKER.NEXT_EAT_AT.next();
	}

	@Override
	public void execute() {
		RSItem[] foodArray = Inventory.find(Vars.food);
		for(RSItem food : foodArray)
		{
			final int currentHealth = Combat.getHP();
			Vars.ANTIBAN.waitNewOrSwitchDelay(Vars.lastBusyTime, true);
			if(food.click("Eat"))
			{
				Timing.waitCondition(new Condition() {
					
					@Override
					public boolean active() {
						return Combat.getHP() > currentHealth;
					}
				}, General.random(1200, 1500));
				Vars.lastBusyTime = Timing.currentTimeMillis();
				
				if(Combat.getHPRatio() > 50)
				{
					Vars.ANTIBAN.INT_TRACKER.NEXT_EAT_AT.reset();
					break;
				}
			}
		}
	}
	
	@Override
	public String toString()
	{
		return "Omnomnom";
	}
}
