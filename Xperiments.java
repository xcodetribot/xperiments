package scripts.Xperiments;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Combat;
import org.tribot.api2007.Game;
import org.tribot.api2007.Skills;
import org.tribot.api2007.Skills.SKILLS;
import org.tribot.script.Script;
import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Painting;

import scripts.Xperiments.Nodes.Antiban;
import scripts.Xperiments.Nodes.AttackExperiment;
import scripts.Xperiments.Nodes.BankItems;
import scripts.Xperiments.Nodes.EatFood;
import scripts.Xperiments.Nodes.TeleportToVarrock;
import scripts.Xperiments.Nodes.UsePotion;
import scripts.Xperiments.Nodes.UseSpecialAttack;
import scripts.Xperiments.Nodes.WalkToBank;
import scripts.Xperiments.Nodes.WalkToExperiments;
import scripts.Xperiments.Utils.Node;
import scripts.Xperiments.Utils.Vars;

@ScriptManifest(authors = { "xCode" }, category = "Combat", name = "Xperiments")
public class Xperiments extends Script implements Painting{

	private ArrayList<Node> nodes = new ArrayList<Node>();
	
	private final long START_TIME = Timing.currentTimeMillis();
	
	private int startDefXP = Skills.getXP(SKILLS.DEFENCE);
	private int startDefLevel = Skills.getActualLevel(SKILLS.DEFENCE);
	private int startAttXP = Skills.getXP(SKILLS.ATTACK);
	private int startAttLevel = Skills.getActualLevel(SKILLS.ATTACK);
	private int startStrXP = Skills.getXP(SKILLS.STRENGTH);
	private int startStrLevel = Skills.getActualLevel(SKILLS.STRENGTH);
	private int startRangeXP = Skills.getXP(SKILLS.RANGED);
	private int startRangeLevel = Skills.getActualLevel(SKILLS.RANGED);
	
	private final Color TRANSP_BLACK_COLOR = new Color(0, 0, 0, 170);
	private final Color TRANSP_GREEN_COLOR = new Color(99, 209, 62, 170);
	private final Font TEXT_FONT = new Font("Arial", 0, 13);
	private final Font TEXT_ITALIC_FONT = new Font("Arial", 2, 11);
	private final Font TITLE_FONT = new Font("Arial", 0, 18);
	
	@Override
	public void run() {
		init();
		
		Vars.food = JOptionPane.showInputDialog("Food name" 
			       ,"");
		
		while(!Vars.stop)
		{
			for (final Node n : nodes) 
			{
				if (n.isValid()) 
				{
					Vars.status = n.toString();
					n.execute();
				}
			}
			sleep(General.random(10, 50));
		}
		
	}

	private void init() {
		nodes.add(new EatFood());
		nodes.add(new AttackExperiment());
		nodes.add(new Antiban());
		nodes.add(new UseSpecialAttack());
		nodes.add(new BankItems());
		nodes.add(new TeleportToVarrock());
		nodes.add(new UsePotion());
		nodes.add(new WalkToBank());
		nodes.add(new WalkToExperiments());
	}

	@Override
	public void onPaint(Graphics gg) {
		Graphics2D g = (Graphics2D)gg;
		
		int gainedStrXP = Skills.getXP(SKILLS.STRENGTH) - startStrXP;
		int gainedAttXP = Skills.getXP(SKILLS.ATTACK) - startAttXP;
		int gainedDefXP = Skills.getXP(SKILLS.DEFENCE) - startDefXP;
		int gainedRangeXP = Skills.getXP(SKILLS.RANGED) - startRangeXP;
		int strlevelsGained = Skills.getActualLevel(SKILLS.STRENGTH) - startStrLevel;
		int attlevelsGained = Skills.getActualLevel(SKILLS.ATTACK) - startAttLevel;
		int deflevelsGained = Skills.getActualLevel(SKILLS.DEFENCE) - startDefLevel;
		int rangelevelsGained = Skills.getActualLevel(SKILLS.RANGED) - startRangeLevel;
		
		long runTimeLong = Timing.currentTimeMillis() - START_TIME;
		String runTime = Timing.msToString(runTimeLong);
		double strxpPH = ((double)gainedStrXP * (3600000.0 / runTimeLong));
		double attxpPH = ((double)gainedAttXP * (3600000.0 / runTimeLong));
		double defxpPH = ((double)gainedDefXP * (3600000.0 / runTimeLong));
		double rangexpPH = ((double)gainedRangeXP * (3600000.0 / runTimeLong));
		
		int paintX = 30;
		int paintY = 300;
		int row2offsetX = 290;
		
		g.setColor(TRANSP_BLACK_COLOR);
		g.fillRect(3, 246, 513, 93);
		
		g.setColor(Color.white);
		
		g.setFont(TITLE_FONT);
		g.drawString("XPeriments", 195, 265);
		
		g.setFont(TEXT_ITALIC_FONT);
		g.drawString("by xCode", 215, 280);
		
		g.setFont(TEXT_FONT);
		
		g.drawString("Runtime: " + runTime, paintX, paintY - 30);
		int yMultiplier = 0;
		if(gainedAttXP > 0)
		{
			g.drawString("Attack: " + gainedAttXP + " XP " + " [" + (int)attxpPH + " P/H]  " + Skills.getActualLevel(SKILLS.ATTACK) + "  [" + attlevelsGained + " levels gained]", paintX, paintY);
			yMultiplier++;
		}
		if(gainedStrXP > 0)
		{
			g.drawString("Strength: " + gainedStrXP + " XP " + " [" + (int)strxpPH + " P/H]  " + Skills.getActualLevel(SKILLS.STRENGTH) + "  [" + strlevelsGained + " levels gained]", paintX, paintY + 15 * yMultiplier);
			yMultiplier++;
		}
		if(gainedDefXP > 0)
		{
			g.drawString("Defence: " + gainedDefXP + " XP " + " [" + (int)defxpPH + " P/H]  " + Skills.getActualLevel(SKILLS.DEFENCE) + "  [" + deflevelsGained + " levels gained]", paintX, paintY + 15 * yMultiplier);
			yMultiplier++;
		}
		if(gainedRangeXP > 0)
		{
			g.drawString("Ranged: " + gainedRangeXP + " XP " + " [" + (int)rangexpPH + " P/H]  " + Skills.getActualLevel(SKILLS.RANGED) + "  [" + rangelevelsGained + " levels gained]", paintX, paintY + 15 * yMultiplier);
			yMultiplier++;
		}
		g.drawString("Status: " + Vars.status, paintX, paintY - 15);
		g.drawString("At experiments: " + Vars.isAtExperiments(), row2offsetX, paintY - 30);
		g.drawString("Experiments area: " + Vars.currentExperimentArea, row2offsetX, paintY - 15);
		//g.setColor(TRANSP_GREEN_COLOR);
		
	}

}
