package scripts.Xperiments.Utils;

import org.tribot.api.Timing;
import org.tribot.api.types.generic.Filter;
import org.tribot.api.util.ABCUtil;
import org.tribot.api2007.Player;
import org.tribot.api2007.types.RSArea;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSNPC;
import org.tribot.api2007.types.RSTile;

public class Vars {

	public static final String 	EXPERIMENT_NAME = "Experiment",
								VARROCK_TELE_NAME = "Varrock teleport";
	public static final String[] ATTACK_POTION_NAMES = new String[]
			{
				"Super attack(4)",
				"Super attack(3)",
				"Super attack(2)",
				"Super attack(1)"
			};
	public static final String[] STRENGTH_POTION_NAMES = new String[]
			{
				"Super strength(4)",
				"Super strength(3)",
				"Super strength(2)",
				"Super strength(1)"
			};
	
	public static boolean 	useAttackPotions = false,
							useStrengthPotions = false;
	public static final ABCUtil ANTIBAN = new ABCUtil();
	public static int currentExperimentArea= -1;
	public static final Filter<RSNPC> EXPERIMENTS_FILTER = new Filter<RSNPC>() {

		@Override
		public boolean accept(RSNPC npc) {
			String npcName = npc.getName();
			return npcName != null && npcName.equals(EXPERIMENT_NAME) && npc.getCombatLevel() == 25 && !npc.isInCombat();
		}
	};
	
	public static boolean stop = false;
	public static String status = "Initialize";
	public static String food = "Shark";
	public static long lastBusyTime = Timing.currentTimeMillis();
	public static int specialAttackCost = 110;
	public static RSArea[] areas = new RSArea[]{
		new RSArea(new RSTile(3535, 9958, 0), new RSTile(3465, 9920, 0)),
		new RSArea(new RSTile(3536, 9922, 0), new RSTile(3580, 9970, 0)),
	};
	
	public static boolean isAtExperiments()
	{
		RSTile playerPos = Player.getPosition();
		for(int i = 0 ; i < areas.length ; i++)
		{
			if(areas[i].contains(playerPos))
			{
				currentExperimentArea = i;
				return true;
			}
		}
		return false;
	}
	
}
